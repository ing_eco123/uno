<?php
	//Datos
	$vp=$_POST["vp"];//Valor presente
	$i=$_POST["i"];//Interes en porcentaje
	$porcentaje=$i/100;//Convierte el interes en decimal
	$np=$_POST["np"];//Numero de Periodos
	//Proceso
	$factor= pow(1+$porcentaje,$np);
	$potencia =number_format($factor,10,".",",");
	$numerador= $porcentaje*$potencia;
	$denominador= $potencia-1;
	$a = $vp*($numerador/$denominador);


	//Formatos
	$np = number_format($np);
	$af = number_format($a,4,".",",");
	$i = number_format($i,2,".",",");
	$vp = number_format($vp,2,".",",");

?>
<html>
	<head>
		<meta http-equiv="Content-type" content="tex/html"; charset="utf-8"/>
		<link rel="stylesheet" href="css/materialize.min.css">

		<style>
			body{
	        background-image: url(img/fondo-sistema6.jpg);
	    	  background-size: 100vw 100vh;
	    	  background-attachment: fixed;
	    	  margin: 0px;
	      }
		</style>
		<script type="text/javascript">
			function boton_back(){
				document.location.href = ""
			}
		</script>
	</head>
	<body><!-- #2D80A4 -->
		<div class="row">

    </div>
		<div class="row">
      <div class="container collection with-header">
        <div class="collection-item">
        <h4 class="collection-header center-align">Cálculo de Serie uniforme equivalente al presente (A)</h4>
				<div class="thumbnail">
					<form method="POST" action="calculadora.php" accept-charset="UTF-8">
					<div class="thumbnail">
						<table class="bordered highlight">
							<tr>
								<th align="center" colspan="2" border=0 class="center-align">Datos</th>
							</tr>
							<tr>
								<td width=53%><b>Valor Presente ($):</b></td>
								<td width=47%><?php echo($vp) ?></td>
							</tr>
							<tr>
								<td width="125"><b>interés (en decimal):</b></td>
								<td width=47%><?php echo($porcentaje) ?></td>
							</tr>
							<tr>
								<td width="125"><b>Número de periodos:</b></td>
								<td width=47%><?php echo($np) ?></td>
							</tr>
							<tr>
								<td width="125"><b>Factor (A/P):</b></td>
								<td width=47%><?php echo($potencia) ?></td>
							</tr>
							<tr>
								<th align="center" colspan="2" border=0 class="center-align">
								Resultado
								</th>
							</tr>
							<tr>
								<td><b>SERIE UNIFORME (A/P)</b></td>
								<td><?php echo($af) ?></td>
							</tr>
							<tr>
							    <?php echo '<center><img src="images/graf3.png" alt="¡Upss! Tu navegador no puede soportar imágenes."  class="responsive-img"></center>'?>
							</tr>
						</table>
					</div>
						<input type="button" value="Nuevo cálculo" onClick="history.back()">
					</div>
				</form>
				</div>
      </div>
      </div>
		</div>
	</body>
</html>
