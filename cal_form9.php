<?php
	//Datos
	$g=$_POST["g"];//Serie de Pagos o Depósitos Uniformes para Acumular un Monto dado a Futuro.
	$porcentaje=$_POST["p"];//Tasa de interés periódica (anual, trimestral, semestral, mensual,…) en %
	$i=$porcentaje/100;//Convierte el interes (%) en una cantidad decimal para usarse en las operaciones
	$np=$_POST["np"];//Número de períodos de capitalización de interés.
	//Proceso
	$factor= pow(1+$i,$np);
	$factorf =number_format($factor,10,".",",");


	$factormenos1= $factorf-1;
	$oper1 = 1/$i;
	$oper2 = $np/$i;
	$oper3 = $i/$factormenos1;
	//
	$a2=$g*($oper1-$oper2*$oper3);

	//Formatos
	$np = number_format($np);
	$a2f = number_format($a2,4,".",",");//mostrar en resultado
	$if = number_format($i,2,".",",");
	$gf = number_format($g,2,".",",");//da formato a line3


?>
<html>
	<head>
		<meta http-equiv="Content-type" content="tex/html"; charset="utf-8"/>
		<link rel="stylesheet" href="css/materialize.min.css">

		<style>
			body{
	        background-image: url(img/fondo-sistema6.jpg);
	    	  background-size: 100vw 100vh;
	    	  background-attachment: fixed;
	    	  margin: 0px;
	      }
		</style>
		<script type="text/javascript">
			function boton_back(){
				document.location.href = ""
			}
		</script>
	</head>
	<body><!-- #2D80A4 -->
		<div class="row">

    </div>
		<div class="row">
      <div class="container collection with-header">
        <div class="collection-item">
        <h4 class="collection-header center-align">Cálculo de Serie anual equivalente al gradiente (A)</h4>
				<div class="thumbnail">
					<form method="POST" action="" accept-charset="UTF-8">
					<div class="thumbnail">
						<table class="bordered highlight">
							<tr>
								<th align="center" colspan="2" border=0 class="center-align">Datos</th>
							</tr>
							<tr>
								<td width=53%><b>Gradiente:</b></td>
								<td width=47%><?php echo($g) ?></td>
							</tr>
							<tr>
								<td width="125"><b>interés (en decimal):</b></td>
								<td width=47%><?php echo($if) ?></td>
							</tr>
							<tr>
								<td width="125"><b>Número de periodos:</b></td>
								<td width=47%><?php echo($np) ?></td>
							</tr>
							<tr>
								<td width="125"><b>Factor (A/G):</b></td>
								<td width=47%><?php echo($factorf) ?></td>
							</tr>
							<tr>
								<th align="center" colspan="2" border=0 class="center-align">
								Resultado
								</th>
							</tr>
							<tr>
								<td><b>SERIE ANUAL EQUIVALENTE AL GRADIENTE (A)</b></td>
								<td><?php echo($a2f) ?></td>
							</tr>
							<tr>
							    <?php echo '<center><img src="images/graf9.png" alt="¡Upss! Tu navegador no puede soportar imágenes."  class="responsive-img"></center>'?>
							</tr>
						</table>
					</div>
						<input type="button" value="Nuevo cálculo" onClick="history.back()">
					</div>
				</form>
				</div>
      </div>
      </div>
		</div>
	</body>
</html>
