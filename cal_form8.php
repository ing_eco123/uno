<?php
	//Datos
	$g=$_POST["g"];//Serie de Pagos o Depósitos Uniformes para Acumular un Monto dado a Futuro.
	$a=$_POST["a"];//Serie de pagos, depósitos o retiros, uniformemente distribuidos en un periodo de tiempo
	$porcentaje=$_POST["p"];//Tasa de interés periódica (anual, trimestral, semestral, mensual,…) en %
	$i=$porcentaje/100;//Convierte el interes (%) en una cantidad decimal para usarse en las operaciones
	$np=$_POST["np"];//Número de períodos de capitalización de interés.
	//Proceso
	$factor= pow(1+$i,$np);
	$factorf =number_format($factor,10,".",",");

	//formula 4
	$numerador= $factorf-1;
	$denominador= $i*$factorf;
	$vp = $a*($numerador/$denominador);
	//formula 7
	$factorxi= $factorf*$i;
	$factormenos1= $factorf-1;
	$oper1 = 1/$i;
	$oper2 = ($factormenos1/$factorxi)-($np/$factorf);
	$vpg = $g*($oper1*$oper2);
	//P =PA+PG
	$p=$vp+$vpg;

	//Formatos
	$np = number_format($np);
	$pf = number_format($p,4,".",",");//mostrar en resultado
	$if = number_format($i,2,".",",");
	$gf = number_format($g,2,".",",");//da formato a line3
	$af = number_format($a,2,".",",");//da formato a line4

?>
    <html>

    <head>
        <meta http-equiv="Content-type" content="tex/html" ; charset="utf-8" />
        <link rel="stylesheet" href="css/materialize.min.css">

        <style>
            body {
                background-image: url(img/fondo-sistema6.jpg);
                background-size: 100vw 100vh;
                background-attachment: fixed;
                margin: 0px;
            }

        </style>
        <script type="text/javascript">
            function boton_back() {
                document.location.href = ""
            }

        </script>
    </head>

    <body>
        <!-- #2D80A4 -->
        <div class="row">

        </div>
        <div class="row">
            <div class="container collection with-header">
                <div class="collection-item">
                    <h4 class="collection-header center-align">Cálculo de Valor Presente Equivalente a un Gradiente</h4>
                    <div class="thumbnail">
                        <form method="POST" action="" accept-charset="UTF-8">
                            <div class="thumbnail">
                                <table class="bordered highlight">
                                    <tr>
                                        <th align="center" colspan="2" border=0 class="center-align">Datos</th>
                                    </tr>
                                    <tr>
                                        <td width=53%><b>Gradiente:</b></td>
                                        <td width=47%>
                                            <?php echo($g) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width=53%><b>Serie:</b></td>
                                        <td width=47%>
                                            <?php echo($a) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="125"><b>interés (en decimal):</b></td>
                                        <td width=47%>
                                            <?php echo($if) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="125"><b>Número de periodos:</b></td>
                                        <td width=47%>
                                            <?php echo($np) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="125"><b>Factor (sin):</b></td>
                                        <td width=47%>
                                            <?php echo($factorf) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th align="center" colspan="2" border=0 class="center-align">
                                            Resultado
                                        </th>
                                    </tr>
                                    <tr>
                                        <td><b>VALOR PRESENTE EQUIVALENTE A UN GRADIENTE</b></td>
                                        <td>
                                            <?php echo($pf) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <?php echo '<center><img src="images/graf8.png" alt="¡Upss! Tu navegador no puede soportar imágenes."  class="responsive-img"></center>'?>
                                    </tr>
                                </table>
                            </div>
                            <input type="button" value="Nuevo cálculo" onClick="history.back()">
                    </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </body>

    </html>
