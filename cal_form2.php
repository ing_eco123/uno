<?php
	//Datos
	$vf=$_POST["vf"];//Valor futuro
	$i=$_POST["i"];//Interes en porcentaje
	$porcentaje=$i/100;//Interes en porcentaje
	$np=$_POST["np"];//Numero de Periodos
	//Proceso
	$factor= pow(1+$porcentaje,$np);
	$potencia =number_format($factor,10,".",",");
	$presente = $vf*(1/$potencia);


	//Formatos
	$np = number_format($np);
	$presentef = number_format($presente,4,".",",");
	$i = number_format($i,2,".",",");
	$vf = number_format($vf,2,".",",");

?>
<html>
	<head>
		<meta http-equiv="Content-type" content="tex/html"; charset="utf-8"/>
		<link rel="stylesheet" href="css/materialize.min.css">

		<style>
			body{
	        background-image: url(img/fondo-sistema6.jpg);
	    	  background-size: 100vw 100vh;
	    	  background-attachment: fixed;
	    	  margin: 0px;
	      }
		</style>
		<script type="text/javascript">
			function boton_back(){
				document.location.href = ""
			}
		</script>
	</head>
	<body><!-- #2D80A4 -->
		<div class="row">

    </div>
		<div class="row">
      <div class="container collection with-header">
        <div class="collection-item">
        <h4 class="collection-header center-align">Cálculo de Valor presente de una cantidad futura (P)</h4>
				<div class="thumbnail">
					<form method="POST" action="calculadora.php" accept-charset="UTF-8">
					<div class="thumbnail">
						<table class="bordered highlight">
							<tr>
								<th align="center" colspan="2" border=0 class="center-align">Datos</th>
							</tr>
							<tr>
								<td width=53%><b>Valor futuro ($):</b></td>
								<td width=47%><?php echo($vf) ?></td>
							</tr>
							<tr>
								<td width="125"><b>interés (en decimal):</b></td>
								<td width=47%><?php echo($porcentaje) ?></td>
							</tr>
							<tr>
								<td width="125"><b>Número de periodos:</b></td>
								<td width=47%><?php echo($np) ?></td>
							</tr>
							<tr>
								<td width="125"><b>Factor (P/F):</b></td>
								<td width=47%><?php echo($potencia) ?></td>
							</tr>
							<tr>
								<th align="center" colspan="2" border=0 class="center-align">
								Resultado
								</th>
							</tr>
							<tr>
								<td><b>PRESENTE (F)</b></td>
								<td><?php echo($presentef) ?></td>
							</tr>
							<tr>
							    <?php echo '<center><img src="images/graf2.png" alt="¡Upss! Tu navegador no puede soportar imágenes."  class="responsive-img"></center>'?>
							</tr>
						</table>
					</div>
						<input type="button" value="Nuevo cálculo" onClick="history.back()">
					</div>
				</form>
				</div>
      </div>
      </div>
		</div>
	</body>
</html>
