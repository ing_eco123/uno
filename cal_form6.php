<?php
	//Datos
	$serie=$_POST["serie"];//Serie de pagos, depósitos o retiros, uniformemente distribuidos en un periodo de tiempo
	$i=$_POST["i"];//Tasa de interés periódica (anual, trimestral, semestral, mensual,…)
	$porcentaje=$i/100;//Convierte el interes en una cantidad decimal para usarse en las operaciones
	$np=$_POST["np"];//Número de períodos de capitalización de interés.
	//Proceso
	$factor= pow(1+$porcentaje,$np);
	$potencia =number_format($factor,10,".",",");

	$numerador= $potencia-1;
	$vf = $serie*($numerador/$porcentaje);


	//Formatos
	$np = number_format($np);
	$vff = number_format($vf,4,".",",");//mostrar en resultado
	$i = number_format($i,2,".",",");
	$serief = number_format($serie,2,".",",");

?>
<html>
	<head>
		<meta http-equiv="Content-type" content="tex/html"; charset="utf-8"/>
		<link rel="stylesheet" href="css/materialize.min.css">

		<style>
			body{
	        background-image: url(img/fondo-sistema6.jpg);
	    	  background-size: 100vw 100vh;
	    	  background-attachment: fixed;
	    	  margin: 0px;
	      }
		</style>
		<script type="text/javascript">
			function boton_back(){
				document.location.href = ""
			}
		</script>
	</head>
	<body><!-- #2D80A4 -->
		<div class="row">

    </div>
		<div class="row">
      <div class="container collection with-header">
        <div class="collection-item">
        <h4 class="collection-header center-align">Cálculo de Valor futuro de una serie uniforme (F)</h4>
				<div class="thumbnail">
					<form method="POST" action="calculadora.php" accept-charset="UTF-8">
					<div class="thumbnail">
						<table class="bordered highlight">
							<tr>
								<th align="center" colspan="2" border=0 class="center-align">Datos</th>
							</tr>
							<tr>
								<td width=53%><b>SerieE ($):</b></td>
								<td width=47%><?php echo($serie) ?></td>
							</tr>
							<tr>
								<td width="125"><b>interés (en decimal):</b></td>
								<td width=47%><?php echo($porcentaje) ?></td>
							</tr>
							<tr>
								<td width="125"><b>Número de periodos:</b></td>
								<td width=47%><?php echo($np) ?></td>
							</tr>
							<tr>
								<td width="125"><b>Factor (F/A):</b></td>
								<td width=47%><?php echo($potencia) ?></td>
							</tr>
							<tr>
								<th align="center" colspan="2" border=0 class="center-align">
								Resultado
								</th>
							</tr>
							<tr>
								<td><b>VALOR FUTURO (F)</b></td>
								<td><?php echo($vff) ?></td>
							</tr>
							<tr>
							    <?php echo '<center><img src="images/graf6.png" alt="¡Upss! Tu navegador no puede soportar imágenes."  class="responsive-img"></center>'?>
							</tr>
						</table>
					</div>
						<input type="button" value="Nuevo cálculo" onClick="history.back()">
					</div>
				</form>
				</div>
      </div>
      </div>
		</div>
	</body>
</html>
